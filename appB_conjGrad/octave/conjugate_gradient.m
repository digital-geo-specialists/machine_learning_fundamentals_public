clear all;


nmax=2 ;  % max number of iterations
tol=0.0000001;   % tolerance
% function to minimize
f=inline("5*x^2 + (1/2)*y^2 - x + y", "x", "y");
% data
x0=0;
y0=0;
b=[1,-1]';
A= [10 0; 0 1];
% initial conditions
x = [x0, y0]';
n=2; % two dimensions
% matrix to store solutions
X=zeros(n,nmax);
X(:,1)=x;
z(1)=f(x(1),x(2));
r = A*x-b
i=1;
p=r;
tol=1.0;
ep=2*tol;
while(ep > tol && i <= nmax)
   alpha=r'*r/(p'*A*p);
   x=x-alpha*p
   z(i)=f(x(1),x(2));

   % save x
   X(:,i)=x;
   % save r
   rp=r;
   r=rp-alpha*A*p
   ep=norm(r)
   beta=r'*r/(rp'*rp);
   
   pp=p; % save previous p
   p=r+beta*pp;
   i=i+1

endwhile   
     



figure;
hold
% draw the contours
Mx=max(X(1,:));
My=max(X(2,:));
mx=min(X(1,:));
my=min(X(2,:));


ngridy=100;
ngridx=100;
dgx = (Mx-mx)/ngridx;
dgy = (My-my)/ngridy;
[U,V] = meshgrid(mx:dgx:Mx, my:dgy:My);
Z=0.5*A(1,1)*U.^2 + 0.5*A(2,2)*V.^2 + 0.5*A(1,2)*U + ...
   0.5*A(2,1)*V-b(1)*U - b(2)*V;
% Z2= (0.5)*[U,V]*A*[U;V] - b'*[U,V]
% Z=U.^2 + 0.5*V.^2 -U + V;
[C,h] = contour(U,V,Z,z);



xlabel('x');
ylabel('y');
title("Steepest descent on f(x,y)");
% colormap('default');



% plot zig-zag lines left right
% line('Color', 'red');
Y=X';
W=reshape(Y(:,1),2,[]);
W(3,:)=nan;
W=W(:);
Q=reshape(Y(:,2),2,[]);
Q(3,:)=nan;
Q=Q(:);
% subplot (2,1,1);
plot(W,Q, 'Color', "blue");
Y=circshift(X',-1);
W=reshape(Y(:,1), 2, []);
W(3,:)=nan;
W=W(:);
Q=reshape(Y(:,2),2,[]);
Q(3,:)=nan;
Q=Q(:);
Q=Q(1:size(Q,1)-2);
W=W(1:size(W,1)-2);
%subplot(2,2,1)

plot(W,Q,'Color',[1,0,0]);
box on
%pbaspect([1,3,1]);
grid on;
axis equal;  % this seems not working
% axis tight;

axis([mx,Mx,my,My]);
ax=gca();
set (ax, "xtick", [0, 0.1,0.2]) 
set (ax, "xticklabelmode", "Manual") 

n=100 ;  % max number of iterations
tol=0.01;   % tolerance
% function to minimize
f=inline("5*x^2 + (1/2)*y^2 - x + y", "x", "y");
% data
x0=0;
y0=0;
b=[1,-1]';
A= [10 0; 0 1];
% initial conditions
X(:,1) = [x0, y0]';
z(1)=f(x0,y0);
R(:,1) = -A*X(:,1)+b;

%steepest descent algorithm
i=2;
while norm( R(:, i-1)) > tol && i<n,
   D=R(:,i-1);
   t=dot(D,D)/dot(A*D,D);
   X(:,i)=X(:,i-1)+t*D;
   R(:,i) = -A*X(:,i)+b;
   z(i)=f(X(1,i), X(2,i));
   i=i+1;
end

% draw the contours
Mx=max(X(1,:));
My=max(X(2,:));
mx=min(X(1,:));
my=min(X(2,:));


ngridy=100;
ngridx=100;
dgx = (Mx-mx)/ngridx;
dgy = (My-my)/ngridy;
[U,V] = meshgrid(mx:dgx:Mx, my:dgy:My);
Z=0.5*A(1,1)*U.^2 + 0.5*A(2,2)*V.^2 + 0.5*A(1,2)*U + ...
   0.5*A(2,1)*V-b(1)*U - b(2)*V;
% Z2= (0.5)*[U,V]*A*[U;V] - b'*[U,V]
% Z=U.^2 + 0.5*V.^2 -U + V;
[C,h] = contour(U,V,Z,z);



xlabel('x');
ylabel('y');
title("Steepest descent on f(x,y)");
% colormap('default');



% plot zig-zag lines left right
% line('Color', 'red');
Y=X';
W=reshape(Y(:,1),2,[]);
W(3,:)=nan;
W=W(:);
Q=reshape(Y(:,2),2,[]);
Q(3,:)=nan;
Q=Q(:);
% subplot (2,1,1);
plot(W,Q, 'Color', [1,0,0]);
Y=circshift(X',-1);
W=reshape(Y(:,1), 2, []);
W(3,:)=nan;
W=W(:);
Q=reshape(Y(:,2),2,[]);
Q(3,:)=nan;
Q=Q(:);
Q=Q(1:size(Q,1)-2);
W=W(1:size(W,1)-2);
%subplot(2,2,1)

plot(W,Q,'Color',[1,0,0]);
box on
%pbaspect([1,3,1]);
grid on;
axis equal;  % this seems not working
% axis tight;

axis([mx,Mx,my,My]);
ax=gca();
set (ax, "xtick", [0, 0.1,0.2]) 
set (ax, "xticklabelmode", "Manual") 


hold off


[C,h] = contour(U,V,Z,z);

