#########
#Example 2.7.1 Pseudo-inverse of a matrix
#########

#Ex 1 Pseudo-Inverse of a row matrix
disp('Ex 1: Pseudo-Inverse of a row matrix')

A=[2,3];
b=5;
A_pseudo=pinv(A)
x=A_pseudo*b

#Ex 2: Pseudo-Inverse of a single column matrix
disp('Ex 2: Pseudo-Inverse of a column matrix')

A=[2;3]            % A is now a single column vector matrix
A_pseudo=pinv(A)
b=[1;2]            % b is now a single column vector
x=A_pseudo*b

# Alternative, graphically inspired solution
disp('Ex 2: Pseudo-Inverse using graphically inspired solution')

u=A/norm(A)  % unit normal
br=(b'*u)*u  % b-transposed and projected
x=br(1)*0.5  % warning: one-based indexing


# Ex 3: A has linearly dependent columns
disp('Ex 3: Pseudo-Inverse of singular matrix')
A=[1 1; 1/2 1/2];
b=[1;3];
R=A(:,1);    % range first column span of R
n=R/norm(R); % normal vector along the range

br=(b'*n)*n   % projection of b into the range of A
N=[1;-1];     % Null space along this vector
Ainv=pinv(A)  % pseudoinverse of A
x=Ainv*b      % pseudosolution
testOrth=x'*N % verification of normality of pseudosolution and null space

# assert that the pseudo solution is normal to null space
assert(testOrth == 0)
