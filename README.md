# Machine Learning for Science and Engineering
# *Volume I - Fundamentals*

**Course Notes Series No. 17**



## About
This repository provides source code and training data for Volume I of the Machine Learning for Science and Engineering series, published 2023 by the Society of Exploration Geophysicists.

This book strives at training a broad audience of students and professionals,  including mathematicians, physicists, geoscientists, and engineers, in the theoretical foundations of Machine Learning (ML). This writing was motivated as a preparation textbook to the course on Artificial Intelligence at the Universidad de Medellin (University of Medellin). It  further evolved over time with experience gained by the authors in teaching and delivering [hands-on workshops](https://digitalgeospecialists.com/wp-content/uploads/2021/03/DGS_brochure_workshop.pdf) to both academia and industry. 

As this is a living document, you may find that the content of the published book may be out-of-sync with the the software found here, although changes should be minor or the new material is augmenting the base code in the book.

If you find any error or have suggestions, please contact the authors.

## Content
ML for science and engineering is written for newcomers to Machine Learning (ML). ML has gained prominence in modern applications in geoscience and related fields, in particular as data sizes and complexity sour exponentially. ML-powered technology increasingly rivals or surpasses human performance and fuels a large range of leading-edge research. This textbook teaches the underlying mathematics, terminology, and programmatic skills to implement, test, and apply ML to real world problems. It builds the mathematical pillars required to thoroughly comprehend and master modern ML concepts and translates the newly gained mathematical understanding into better applied data science. Exercises with raw field data such as well logs and weather measurements prepare and encourage the reader to start using software to validate results, and to program their own creative data solutions. Most importantly, the reader always keeps an eye on the ML’s imperfect data situations as encountered in the real world. 

## Software
The Python code in the book was tested with Python 3.7, although versions larger than 3.5 should work just fine. You find more information and the newest distributions on the [Python site.](https://www.python.org/) We typically work in an [Anaconda environment](https://www.anaconda.com/) and use [Jupyter notebooks](https://jupyter.org/) or the [Spyder IDE](https://www.spyder-ide.org/). Detailed instructions on installing the Python environment is given in the installation folder.

Octave is a freely available software that is compatable with Matlab. Please visit [the Octave home page](https://www.octave.org)  for details. 

## Authors
**Herman Jaramillo Villegas**, Universidad de Medellin

I have a bachelor’s degree in mathematics from Universidad Nacional de Colombia. After finishing my master’s and PhD degrees at Colorado School of Mines I worked in the oil services industry from 1998 until 2016 in Houston, TX. From 2017 until today I have been working as a professor at Universidad de Medellín Colombia teaching Multidimensional Calculus, Numerical Analysis, Machine Learning, and Deep Learning.

**Andreas R&uuml;ger**, [Digital Geo Specialists LLC](https://digitalgeospecialists.com), 

I have 30 years of geoscience industry experience, including work as a Chief Geophysicist, Adjunct Professor and book author.  I can help develop world-class geophysical software, perform advanced seismic data analysis and teach advanced scientific topics. My areas of expertise are Machine Learning, velocity model building, seismic anisotropy, seismic imaging and processing. 

## License
The software in this repository, including all code samples in the notebooks listed above, is released under the BSD license described more thoroughly in the LICENCE file.

## Impressum
The text content of the book is published 2023 by the [Society of Exploration Geophysicists (SEG)] (https://library.seg.org/) in the SEG Course Notes Series.
The volume editor is **Umair bin Waheed**.

* ISBN 0-931830-48-6 (Series)
* ISBN 978-1-56080-338-1 (Volume) 
* Library of Congress Control Number: 2022945611
