# Litho-Facies data

Please load the litho-facies dataset(s) directly from the [SEG's 2016 tutorial GitHub page](https://github.com/seg/tutorials-2016/tree/master/1610_Facies_classification) for the ```Facies classification using machine learning``` Python project.

Link:
https://github.com/seg/tutorials-2016/tree/master/1610_Facies_classification
