% Example 3.3.6

% Verify the one-hidden layer network to solve the XOR problem

% definition of the sigmoid function
S=@(x) (1+e.^(-x)).^(-1)

% definition of the X matrix for input samples  [1 1] [1 0] [0 1] [0 0]
X=[1 1; 1 0; 0 1; 0 0];

% with bias term
X= [ones(size(X,1),1),  X]

% Weight matrices
Theta1=[-10 30; 20 -20 ; 20 -20]
Theta2=[-30; 20; 20]

% Hidden layer
Z2=X*Theta1
A2=S(Z2)


% find output h(Theta,X)
A2=[ones(size(A2,1),1), A2]
Z3=A2*Theta2
H=S(Z3)

B=(H>0.5)  % this corresponds to the XOR output
