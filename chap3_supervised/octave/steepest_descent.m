tx=linspace(-1,1,50)';
ty=linspace(-2,0,50)';
[x,y]=meshgrid(tx,ty);
z=5*x.^2 + y.^2/2 - x + y;
colormap("default");
[Fx, Fy] = gradient(z);
surf(z, Fx+Fy);
shading interp;
xlabel("x");
ylabel("y");
zlabel("z");
title=("paraboloid for steepest descent");




contourf(x,y,z, -1:9);

clear all;


n=100 ;  % max number of iterations
tol=0.01;   % tolerance
% function to minimize
f=inline("5*x^2 + (1/2)*y^2 - x + y", "x", "y");
% data
x0=0;
y0=0;
b=[1,-1]';
A= [10 0; 0 1];
% initial conditions
X(:,1) = [x0, y0]';
z(1)=f(x0,y0);
R(:,1) = -A*X(:,1)+b;

%steepest descent algorithm
i=2;
while norm( R(:, i-1)) > tol && i<n,
   D=R(:,i-1);
   t=dot(D,D)/dot(A*D,D);
   X(:,i)=X(:,i-1)+t*D;
   R(:,i) = -A*X(:,i)+b;
   z(i)=f(X(1,i), X(2,i));
   i=i+1;
end

   

figure;
hold
% draw the contours
Mx=max(X(1,:));
My=max(X(2,:));
mx=min(X(1,:));
my=min(X(2,:));


ngridy=100;
ngridx=100;
dgx = (Mx-mx)/ngridx;
dgy = (My-my)/ngridy;
[U,V] = meshgrid(mx:dgx:Mx, my:dgy:My);
Z=0.5*A(1,1)*U.^2 + 0.5*A(2,2)*V.^2 + 0.5*A(1,2)*U + ...
   0.5*A(2,1)*V-b(1)*U - b(2)*V;
% Z2= (0.5)*[U,V]*A*[U;V] - b'*[U,V]
% Z=U.^2 + 0.5*V.^2 -U + V;
[C,h] = contour(U,V,Z,z);



xlabel('x');
ylabel('y');
title("Steepest descent on f(x,y)");
% colormap('default');



% plot zig-zag lines left right
% line('Color', 'red');
Y=X';
W=reshape(Y(:,1),2,[]);
W(3,:)=nan;
W=W(:);
Q=reshape(Y(:,2),2,[]);
Q(3,:)=nan;
Q=Q(:);
% subplot (2,1,1);
plot(W,Q, 'Color', [1,0,0]);
Y=circshift(X',-1);
W=reshape(Y(:,1), 2, []);
W(3,:)=nan;
W=W(:);
Q=reshape(Y(:,2),2,[]);
Q(3,:)=nan;
Q=Q(:);
Q=Q(1:size(Q,1)-2);
W=W(1:size(W,1)-2);
%subplot(2,2,1)

plot(W,Q,'Color',[1,0,0]);
box on
%pbaspect([1,3,1]);
grid on;
axis equal;  % this seems not working
% axis tight;

axis([mx,Mx,my,My]);
ax=gca();
set (ax, "xtick", [0, 0.1,0.2]) 
set (ax, "xticklabelmode", "Manual") 


hold off


plot(z, 'LineWidth', 2)
box on;
grid on;
xlabel('iteration number');
ylabel('Cost function z');

% write 3D points for gnuplot
fp = fopen('gnuplot.dat','w');
for i = 1: size(z,2)
    fprintf(fp,'%f %f %f \n', X(1,i),X(2,i),z(i));       
end


fclose(fp);

fp = fopen('gnuplot0.dat','w');
for i = 1: size(z,2)
    fprintf(fp,'%f %f %f \n', X(1,i),X(2,i),0);       
end


fclose(fp);



