% load a simple data set
load HousePrices.mat;
who;  % this list the variables in the data

% start vectors X and Y
X = HouseSize(:);
y = HousePrice(:);
XY=[X'; y'];
fprintf('%8g   %8g\n', XY) % print the two vectors X and Y


% add ones to X
X=[ones(size(X,1),1) X]

% find the X^T X matrix
XTX = X'*X

% invert the XTX matrix
XTXInv = inv(XTX)

% find b
b=X'*y

% find theta
theta=XTXInv*b

% note that all this could be solved with just one Octave instruction
theta2 = pinv(X)*y

% scatter(HouseSize, HousePrice, [100]) % plot points
xlabel("House Size")
ylabel("House Price")
hold on;  % wait, more things to come for the plot
plot( HouseSize, HousePrice, 'rx' ,  X(:,2), X*theta, '-');  % plot fitting line
legend('Traning data', 'Linear regression data');

