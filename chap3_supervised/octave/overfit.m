% define function that builds up the "Vandermonde" matrix
function W=buildmat(X,n)
    for i=1:n;
        W(:,i)=X.^(i-1);
    endfor  
end

% function to find theta=(X+lambda I)^(-1) y
function t=findtheta(X,y,lambda)
    n=size(y,1);
    t=pinv(X + lambda*eye(n))*y;
end

function t=findtheta2(X,y)
    n=size(y,1);
    t=pinv(X)*y;
end



% function to build polynomial solution
function Y=findY(theta,X,n,m)
    for i=1:n;
        s=0;
        for j=1:m;
           s=s + (X(i)^(j-1))*theta(j);
           Y(i)=s;
        endfor
    endfor
end
       
    


n=4;
Xdom=[0.25; 0.4; 0.5; 0.7];
Y=[0.15; 0.3; 0.1; 0.4];
X=zeros(n);
% build Vandermonde matrix
X=buildmat(Xdom,n);

% find theta
lambda=0;
theta=zeros(n,1);
theta=findtheta(X,Y,lambda);

% build polynomial solution
n2=100;
Xnewdom=linspace(0.25,0.7,n2);
Ynew=zeros(n2,1);
Ynew=findY(theta,Xnewdom,n2,n);

% linear regression
n=2;
X=zeros(n);
X=buildmat(Xdom,n);
% find theta
lambda=0;
theta=zeros(n,1);
theta=findtheta2(X,Y);

% build linear
n2=100;

Ynew2=zeros(n2,1);
Ynew2=findY(theta,Xnewdom,n2,n);




% plot
hold on;
xlabel('x');
ylabel('y');
plot(Xdom, Y, 'o', "markersize",10, "markerfacecolor", "blue");
% set all line widths to 10 pts
set(0, "defaultlinelinewidth", 5);
plot(Xnewdom,Ynew2);
plot(Xnewdom,Ynew);
h=legend("Original data", "linear regression", "polynomial interpolation");
legend (h, "location", "northwestoutside");


hold off;


% find regression with four values of lambda=0.01,0.1,0.5,1.0
n=4;
X=buildmat(Xdom,n);

% find theta
lambda=0.01;
theta=zeros(n,1);
theta=findtheta(X,Y,lambda);

% build polynomial solution

Ynew3=zeros(n2,1);
Ynew3=findY(theta,Xnewdom,n2,n);

lambda=0.1;
theta=zeros(n,1);
theta=findtheta(X,Y,lambda);

% build polynomial solution

Ynew4=zeros(n2,1);
Ynew4=findY(theta,Xnewdom,n2,n);

lambda=0.5;
theta=zeros(n,1);
theta=findtheta(X,Y,lambda);

% build polynomial solution

Ynew5=zeros(n2,1);
Ynew5=findY(theta,Xnewdom,n2,n);


lambda=1.0;
theta=zeros(n,1);
theta=findtheta(X,Y,lambda);

% build polynomial solution

Ynew6=zeros(n2,1);
Ynew6=findY(theta,Xnewdom,n2,n);


% plot
hold on;
xlabel('x');
ylabel('y');
plot(Xdom, Y, 'o', "markersize",10 ,"markerfacecolor", "blue");
plot(Xnewdom,Ynew);
plot(Xnewdom,Ynew2);



plot(Xnewdom,Ynew3);
plot(Xnewdom,Ynew4);
plot(Xnewdom,Ynew5);
plot(Xnewdom,Ynew6);

h=legend("Original data","polynomial: lambda=0", " linear regression", "lambda=0.01", "lambda=0.1", ...
  "lambda=0.5", "lambda=1.0");
legend (h, "location", "northwestoutside");
hold off;
