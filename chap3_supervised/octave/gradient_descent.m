clear all;

tx=ty= linspace(-pi,pi,50)';
[x,y]=meshgrid(tx,ty);

%z=cos(x).*cos(y).*exp(-x.^2 - y.^2);

z=sin(x).*sin(y);

% mesh(tx,ty,z);

colormap("default");
[Fx, Fy] = gradient(z);
surf(x,y,z, Fx+Fy);
shading interp;
xlabel("x");
ylabel("y");
zlabel("z");
% axis([-1.5,1.5,-1.5,1.5]);
title=("multimodal steepest descent");




contourf(x,y,z);

clear all;


n=20 ;  % max number of iterations
% tol=0.01;   % tolerance, not used here
% function to minimize

f=inline("sin(x)*sin(y)", "x", "y");
% data
theta=zeros(n,2);
alpha=0.4;


%gradient
gx= ...
   inline("cos(x)*sin(y)", "x","y");
gy= ...
   inline("sin(x)*cos(y)","x","y");
   
% initial conditions



%steepest descent algorithm
i=2;
theta(1,1)=1.3;
theta(1,2)=0.5;
while  i<=n,
   gxt=gx(theta(i-1,1), theta(i-1,2));
   gyt=gy(theta(i-1,1), theta(i-1,2));
   theta(i,:)=theta(i-1,:) - alpha*[gxt,gyt];
   
   i=i+1;
end

   

% write 3D points for gnuplot
fp = fopen('graddesc.dat','w');
for i = 1: size(theta,1)
    x=theta(i,1);
    y=theta(i,2);
    fprintf(fp,'%f %f %f \n', x, y, f(x,y));       
end


fclose(fp);

fp = fopen('graddesc0.dat','w');
for i = 1: size(theta,1)
    x=theta(i,1);
    y=theta(i,2);
    fprintf(fp,'%f %f %f \n', x, y, -1.5);       
end


fclose(fp);

