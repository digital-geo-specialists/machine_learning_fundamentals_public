#!/bin/sh

# next instruction will generate a latex error. Please keep pushing enter.
# The reason is that the file "bowl_contourtmp0.table" is not yet there
pdflatex bowl
# this generates the script "bowl_countourtmp0.table"
gnuplot bowl_contourtmp0.script
# now the file "bowl_contourtmp0.table"  is there. Contours in the surface will be created
pdflatex bowl

# display the figure, assuming you have okular in your system
okular bowl.pdf
