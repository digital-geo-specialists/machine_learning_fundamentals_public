# Installing Octave

[GNU Octave](https://octave.org/) is a free scientific programming language package that is compatable with the commercial [Matlab](https://www.mathworks.com/products/matlab.html) application. It is licensed under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html) (GPL) and can be run for academic or commercial purposes.

You can download Octave versions for all main platforms at https://octave.org/download. Unlike the Python install, there is no need to install a virtual environment or, in most cases, install additional libraries. The install will require about 300 megabytes and is straightforward.

As of the time of writing these notes, we use GNU Octave version 7.3.0.

Reference:
```
John W. Eaton, David Bateman, Søren Hauberg, Rik Wehbring (2022).
GNU Octave version 7.3.0 manual: a high-level interactive language for numerical computations.
URL https://www.gnu.org/software/octave/doc/v7.3.0/'''