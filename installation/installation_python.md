# Python Installation 

There are many ways to install and use [Python](https://www.python.org) on Windows, macOS, Linux or other computational platforms. We suggest to set up a dedicated Python environment with libraries required to execute the code samples referenced in the book. Our Python codes are presented in [Jupyter notebooks](https://jupyter.org/) that allow editing, documenting and running the software via a web browser in [JupyterLab](https://jupyterlab.readthedocs.io/). Other alternatives include using [PyDev for Eclipse](https://www.pydev.org/), [PyCharm](https://www.jetbrains.com/pycharm/), [Spyder](https://www.spyder-ide.org/) and more.


Below is a listing of mandatory and optional libraries (or packages), followed by recommended installation instructions.

* Python version: Python 3.7 or higher
* Required Libraries: scipy, matplotlib, pandas, seaborn, scikit-learn
* Optional libraries:  segyio, scikit-image, keras, ipympl, ipywidgets 

## Setup your virtual Python with Anaconda:

For non-commercial use, we recommend installing the popular [Anaconda Python data-science platform](https://www.anaconda.com) for the book examples. The Anaconda application simplifies package management, deployment and development. It is a user-friendly application build and its name indicates that it is build on top of a popular open-source environment management system called [Conda](https://docs.conda.io/en/latest/). A free version of Anaconda can be downloaded at https://www.anaconda.com/products/individual. A comprehensive user guide is provided at https://docs.anaconda.com/anaconda/user-guide. As of the time of writing these notes, we use version 2.3.2 of the Anaconda Navigator on a Window 11 computer.


A standard Anaconda install ships with a Python installation (here: Python 3.9.11). We require a Python 3.7 (or higher) version, and the source code will not run on Python 2.X. 

>![Anaconda Navigator](anaconda_navigator.png)

### Managing your Python environment with Conda
You can create a Python environment directly in the Anaconda Navigator as described in the user [guide](https://docs.anaconda.com/anaconda/user-guide/). 

For those who try to gain more inside into software package management, we  recommend using an official package manager such as ```pip```, ```virtualenv```, or  [Conda](https://conda.io). We prefer using the later, Conda, to assemble a dedictated Python environment for experimenting with the book examples. For once, Anaconda is built on top of Conda. In addition, Conda appears to be more rigorous in ensuring that the various data science packages are mutually compatable. Conda commands can be executed from both the ```Anaconda CMD``` or ```Powershell``` Prompt windows (center items in the Anaconda Navigator image above): 
	
The following Conda instructions create a simple Python environment called ```ML_book```, and installs the popular ```pandas``` package:
```
conda create --name ML_book
conda activate ML_book
conda install pandas
conda deactivate ML_book
``` 
The first line creates the ``ML_book`` environment. This is only required once and creates a new directory in your workflow folder. Programmatically, you can query your created program environments using the command ```conda env list```. 
The second line switches the shell into the ```ML_book``` environment. Finally, the pandas package is installed and the environment deactivated until further use.


-----


## Verify the install 

We highly recommend verifying both Python and Conda installations. Again, we suggest using the ```Anaconda Prompt``` application as a simple test platform. This application is part of the Anaconda installation and can be found, for example, by typing ```Anaconda Prompt``` in the Windows' search field or click the corresponding option in the Navigator. 

### Conda verification
Simply query the Conda version number as a flame-test that conda is installed. At the prompt, type
```
conda -V
``` 
This should return *conda 22.9.0" or similar*


### Python verification
Assuming that the ```ML_book``` environment has been created, simply query the version numbers of Python after activating *ML-book*


```
conda activate ML_book
python -V
``` 
This returns the Python version that is used in this particular environment. Please note that different Python version can be used in each environment and that the Python version provided by Anaconda will most likely differ from the default Python installation on your computer.


### Library install and verification

SciPy is a bundle of data scientific computing libraries and a workhorse package for Machine Learning applications in Python. Probably the most important item in the SciPy bundle is the NumPy library that facilitates working with arrays. Lets install scipy and test if its NumPy library is properly installed:

```
conda activate ML_book
conda install scipy
conda deactivate ML_book
``` 
You can test the scipy version in the Python application as shown in this screenshot. 
>![](verify_numpy.png)

There are alternative ways to verify the fidelity of imported libraries. For example, as SciPy uses NumPy underneath, you can test the numpy install explicitely with instructions such as

```
import numpy as np
a_array = np.array([1,2,3])
assert ( np.max(a_array) == 3 ) , "Error"
```
-----
Executing the following lines of code in the new Python environment should succeed, or an ```AssertionError``` will be thrown and the program terminated.

## Other useful Conda commands

**Delete an existing environment**
```
conda env remove -n  ML_book
```

**List libraries in an existing environment**
```
conda list -n  ML_book
```

## Python development in Anaconda
After establishing the python runtime, you are now ready to develop and execute Python code.


Reference:
```
Van Rossum, G. (2007). 
Python programming language. 
In USENIX Annual Technical Conference. 
URL https://www.gnu.org/software/octave/doc/v7.3.0/'''